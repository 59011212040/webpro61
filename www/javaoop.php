<!DOCTYPE HTML>
<html>
	<body>
		<script>
		function makeUser(name, age) {
 		return {
    	name: name,
    	age: age
    };
}
		let user = makeUser("John", 30);
		alert(user.name); // John
		</script>
	</body>
</html>